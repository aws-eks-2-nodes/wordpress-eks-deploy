resource "kubernetes_namespace" "wp_namespace" {
  metadata {
    name = "wp-namespace"
  }
}

data "aws_ebs_volume" "wordpress_volume_db" {
  most_recent = true

  filter {
    name   = "volume-type"
    values = ["gp3"]
  }

  filter {
    name   = "tag:wp-k8s"
    values = ["database"]
  }
}


resource "kubernetes_storage_class" "wp-storage" {
  metadata {
    name = "wp-storage"
 
    labels = {
       name = "wp-db"
       app = "wordpress_db"
    }
  }
  storage_provisioner = "kubernetes.io/aws-ebs"
  reclaim_policy      = "Delete"
  parameters = {
    type = "gp3"
  }
  volume_binding_mode = "Immediate"
  
}

resource "kubernetes_secret" "wordpress_db_secret" {
    metadata {
        name      = "wordpress-db-pwd"
        namespace = "wp-namespace"
          labels = {
       name = "wp-db"
       app = "wordpress_db"
    }
    }
 
    data = {
        username = "wordpress-db"
        wordpress-pwd   = "cEBzc3dvcmQxMjM="
    }
       type = "kubernetes.io/basic-auth"
}

resource "kubernetes_persistent_volume" "wp_db_persistent_volume" {
  metadata {
    name = "mysql-pv"
   
    labels = {
       name = "wp-db"
       app = "wordpress_db"
    }
    
  }
  spec {
    storage_class_name = "wp-storage"
    capacity = {
      storage = "20Gi"
    }
    access_modes = ["ReadWriteOnce"]
    persistent_volume_source {
        aws_elastic_block_store {
           volume_id = data.aws_ebs_volume.wordpress_volume_db.id

        }

    }

  }
  #depends_on = [aws_ebs_volume.wordpress_volume]
}


resource "kubernetes_persistent_volume_claim" "wp_db_persistent_volume_claim" {
  metadata {
    name = "wp-db-presistentclaim"
    namespace = "wp-namespace"
    labels = {
       name = "wp-db"
       app = "wordpress_db"
    }
  }
  spec {
    storage_class_name = "wp-storage"
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "20Gi"
      }
    }
    selector {
      match_labels = {
         name = "wp-db"
         app = "wordpress_db"
      }  
    }
    volume_name = "${kubernetes_persistent_volume.wp_db_persistent_volume.metadata.0.name}"
  }
}

resource "kubernetes_service" "wp-mysql" {
  metadata {
    name = "wordpress-mysql"
        namespace = "wp-namespace"
    labels = {
       name = "wp-db"
       app = "wordpress_db"
    }
  }
  spec {
    selector = {
      app = "wordpress_db"
    }
    #session_affinity = "ClientIP"
    port {
      port        = 3306
      #target_port = 80
    }

    #type = "LoadBalancer"
  }
}

resource "kubernetes_deployment" "wordpress_db" {
  metadata {
    name      = "wp-db-deployment"
    namespace = kubernetes_namespace.wp_namespace.metadata.0.name
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "wordpress_db"
        tier = "backend"
      }
    }

    template {
      metadata {
        labels = {
          app = "wordpress_db"
          tier = "backend"
        }
      }
      spec {
        container {
          image = "mysql:8.4"
          image_pull_policy = "Always"
          name  = "mysql"
          port {
            container_port = 3306
            name = "mysql"
          }
                env {
               name = "MYSQL_DATABASE"
              value = "wordpressdb"
            }
          env {
           name = "MYSQL_USER"
           value = "wordpress-db-user"
            }
           env {
           name = "MYSQL_PASSWORD"
           value_from {
              secret_key_ref {
                name = kubernetes_secret.wordpress_db_secret.metadata[0].name
                key = "wordpress-pwd"
              } 
           }
            }
            env {
           name = "MYSQL_ROOT_PASSWORD"
           value_from {
              secret_key_ref {
                name = kubernetes_secret.wordpress_db_secret.metadata[0].name
                key = "wordpress-pwd"
              } 
           }
            }

           env {
               name = "WORDPRESS_DB_HOST"
              value = "wordpress-mysql"
            }

          volume_mount {
            name = "wordpress-persistent-storage"
            mount_path =  "/var/lib/mysql"
          }

        }
        volume{
          name = "wordpress-persistent-storage"
          persistent_volume_claim {
            claim_name = "wp-db-presistentclaim"
          }
        }
        toleration {
          key = "wp-k8s"
          operator = "Equal"
          value = "database"
          effect = "NoSchedule"
        }
      }
    }
  }
   depends_on = [kubernetes_persistent_volume_claim.wp_db_persistent_volume_claim]
}
