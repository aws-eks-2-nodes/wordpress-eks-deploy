#!/bin/bash
instanceid=$(aws ec2 describe-instances --region ap-south-1 --filters Name=tag:eks:nodegroup-name,Values=sandbox-workernodes-db --query "Reservations[0].Instances[0].InstanceId" --output text)

az=$(aws ec2 describe-instances --instance-id $instanceid --region ap-south-1 --query "Reservations[0].Instances[0].Placement.AvailabilityZone" --output text)
volume=$(aws ec2 describe-volumes --region ap-south-1 --filters Name=tag:wp-k8s,Values=database --output text )

if [$volume == ""]
then
        echo "Creating Volume in $az"
        aws ec2 create-volume  --volume-type gp3 --size 25 --region ap-south-1 --availability-zone $az --tag-specifications 'ResourceType=volume,Tags=[{Key=wp-k8s,Value=database},{Key=Name,Value=pv-database}]'
else
        echo "volume already created"
fi